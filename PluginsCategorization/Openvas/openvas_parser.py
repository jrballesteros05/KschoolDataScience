""" Every plugin from openvas
is a script that run in the respective machine but it also provides relevant information
for our purposes, like the solution, cve, cvss and so on. This module download (or script)
download information from OpenvasWebSite, filter the relevant information and create a
custom dataset in json format."""
import os
import re
import csv
import json
from openvas_downloader import download_file, decompress_plugin_file

def read_vendor_reference_csv(filename):
    """
    Read vendor references from file and saves into list this function is useful
    to enrich vulnerability data
    Parameters:
    ----------
    filename : read a filename with regular expressions

    Returns:
    -------
    reference_regex_csv : list with vendor references regex
    """
    reference_regex_csv = []
    with open(filename, 'r') as vendors:
        reader = csv.reader(vendors)
        next(reader, None)  # skip the headers
        for row in reader:
            reference_regex_csv.append(row[2])
    return reference_regex_csv

def look_for_data(filecontent, filename):
    """
    Function that look for OID first and then look for other fields
    Parameters:
    ----------
    filecontent : Read the content of the file
    filename : filename to read

    Returns:
    -------
    plugin_data : Json line with extracted fields in "generate_dataset" function
    """
    oid = re.compile(r'(script_oid|SCRIPT\_OID).*?\".*?\.(?P<oid>\d+)\"')
    plugin_data = "None"
    lookforoid = oid.search(filecontent.decode("ISO-8859-1"))
    if lookforoid:
        plugin_data = generate_dataset(filecontent, lookforoid.group('oid'), filename)
    else:
        print(filename)
    return plugin_data

def write_dataset_json(filename, outputlist):
    """
    Write dataset into json file
    Parameters:
    ----------
    filename : filename to read
    outputlist: json list
    """
    with open(filename, "w") as outjson:
        outjson.write(json.dumps(outputlist) + "\n")

def clean_text(text):
    """
    This function clean rubbish from "text" field in our custom
    dataset.
    Parameters:
    ----------
    text : string text
    """
    text = text.replace('if(description)'.replace(' ', '').strip(), '').replace('{', '')
    text = text.replace('if(description)', '').replace('{', '')
    text = re.sub('if (description)', '', text)
    text = re.sub(r'#.*', '', text)
    text = re.sub(r'exit\(\d+\).*\n\}(.*?\n)+', '', text)
    text = re.sub(r'\#+', '', text)
    text = re.sub(r'.*?51.*?Franklin.*?St.*', '', text)
    text = re.sub(r'MA 02110-1301 USA', '', text)
    text = re.sub(r'th Floor, Boston, .', '', text)
    text = re.sub(r'Franklin St, Fif', '', text)
    text = re.sub(r'^\n+', '', text)
    text = text.strip()
    return text

def match_solution(filecontent):
    """
    Look for different kind of solutions.
    Parameters:
    ----------
    filecontent : Read the content of the file

    Returns:
    -------
    solution : string with the solution of vulnerability
    """
    solution_regex = re.compile(r'script\_tag\(name.*?\"solution\".*?value.*?\:(?P<solution>.*)')
    solution_ml_regex = re.compile(r'name:.*?solution\".*?value\:.*?'
                                   r'\"(?P<solution_ml>[\s\S]*?)\"\)',
                                   re.MULTILINE)
    tag_solution_regex = re.compile(r'tag\_solution.?\=.*?\"(?P<tag_solution>.*)')
    tag_solution_ml = re.compile(r'tag\_solution.*?\=.*?((\n+\")|(\"))'
                                 r'(?P<tag_sol_multiline>.*\n+.*\n+)'
                                 , re.MULTILINE)
    solution = None
    if solution_regex.search(filecontent):
        solution = solution_regex.search(filecontent).group('solution')
    if tag_solution_regex.search(filecontent):
        solution = tag_solution_regex.search(filecontent).group('tag_solution')
    if tag_solution_ml.search(filecontent):
        solution = tag_solution_ml.search(filecontent).group('tag_sol_multiline')
    if solution_ml_regex.search(filecontent):
        solution = solution_ml_regex.search(filecontent).group('solution_ml')
    return solution

def match_scriptname(filecontent):
    """
    Find the scriptname from different regex
    Parameters:
    ----------
    filecontent : Read the content of the file

    Returns:
    -------
    plugin_name : string with the name of the plugin
    """
    scriptname = re.compile(r'script\_name\(.*?(\"|\')(?P<script_name>.*?)(\"|\').*?\)')
    scriptname_alternative = re.compile(r'name.*?\=.*?(\"|\')(?P<script_name>.*?)(\"|\')')
    scriptname_alt = re.compile(r'script\_name\(\"(?P<script_name>.*?)\"\)')
    plugin_name = None
    if scriptname_alt.search(filecontent):
        plugin_name = scriptname_alt.search(filecontent).group('script_name')
    elif scriptname.search(filecontent):
        plugin_name = scriptname.search(filecontent).group('script_name')
    elif scriptname_alternative.search(filecontent):
        plugin_name = scriptname_alternative.search(filecontent).group('script_name')
    plugin_name = plugin_name.replace(',', ' ')
    plugin_name = plugin_name.replace('\"', '')
    plugin_name = plugin_name.replace("\'", '')
    return plugin_name

def match_solution_type(cvss, filecontent):
    """
    Function that puts informational solution into solution_type field
    Parameters:
    ----------
    cvss : get the cvss score from the plugin
    filecontent : Read the content of the file

    Returns:
    -------
    solution_type : string with the solution_type of the plugin
    """
    solution_type_regex = re.compile(r'solution\_type\".*?value.*?\"(?P<solution_type>.*?)\"')
    solution_type = None
    if cvss == '0.0':
        solution_type = "Informational"
    else:
        if solution_type_regex.search(filecontent):
            solution_type = solution_type_regex.search(filecontent).group('solution_type')
    return solution_type

def match_general(filecontent, group_to_match):
    """
    General function to match any regular expression
    Parameters:
    ----------
    filecontent : Read the content of the file
    group_to_match : Regular expression to match

    Returns:
    -------
    result : string with the group value of regex
    """
    regexcompiled = None
    result = None
    if group_to_match == 'script_category':
        regexcompiled = re.compile(r'script\_category\((?P<script_category>.*?)\)')
    if group_to_match == 'script_family':
        regexcompiled = re.compile(r'script\_family\((?P<script_family>.*?)\)')
    if group_to_match == 'creation_date':
        regexcompiled = re.compile(
            r'creation_date\"\,.*?value\:\"(?P<creation_date>\d+\-\d+\-\d+\s+\d+\:\d+\:\d+)')
    if group_to_match == 'last_modification':
        regexcompiled = re.compile(r'last_modification\"\,.*?value\:\".*?'
                                   r'(?P<last_modification>\d+\-\d+\-\d+\s+\d+\:\d+\:\d+)')
    if group_to_match == 'cvss_base':
        regexcompiled = re.compile(r'\"cvss_base\".*value\:.*?\"(?P<cvss_base>.*?)\"')
    if group_to_match == 'cvss_vector':
        regexcompiled = re.compile(r'\"cvss_base\_vector\"\,.*?value.*?\:.*?'
                                   r'\"(?P<cvss_vector>.*?)\"')
    if regexcompiled.search(filecontent):
        result = regexcompiled.search(filecontent).group(group_to_match)
        result = result.replace('\"', '').strip()
    return result

def generate_dataset(filecontent, oid, filename):
    """
    Function to create "final" dataset
    Parameters:
    ----------
    filecontent : Read the content of the file
    oid : unique value id from the plugin
    filename : plugin file to parse

    Returns:
    -------
    jsondata : dictionary line with the extracted fields
    """
    cve_regex = re.compile(r'(?P<cve_data>CVE\-\d+\-\d+)')
    filecontent = filecontent.decode("ISO-8859-1")
    cve = cve_regex.findall(filecontent)
    plugin_name = match_scriptname(filecontent)
    solution = match_solution(filecontent)
    creation_date = match_general(filecontent, 'creation_date')
    last_modification = match_general(filecontent, 'last_modification')
    script_category = match_general(filecontent, 'script_category')
    script_family = match_general(filecontent, 'script_family')
    cvss = match_general(filecontent, 'cvss_base')
    cvss_vector = match_general(filecontent, 'cvss_vector')
    solution_type = match_solution_type(cvss, filecontent)
    filecontent = clean_text(filecontent)
    jsondata = {"oid": oid, "creation_date": creation_date,
                "last_modification_date":last_modification,
                "plugin_name": plugin_name, "filename": filename,
                "script_category": script_category, "script_family": script_family,
                "cve": cve,
                "cvss": cvss, "cvss_vector": cvss_vector,
                "solution_from_plugin": solution,
                "solution_type": solution_type, "text": filecontent}
    return jsondata

def walk_directories(directory, outputfile):
    """
    Function to walk for every single file on specific directory
    Parameters:
    ----------
    directory : Directory with the openvas plugins
    outputfile : json file to write dataset
    """
    oids_list = []
    print("Waling in root directory")
    for root, subdirs, files in os.walk(directory):
        print("Walking in those subdirectories: %s" % subdirs)
        #Files walking
        for file in files:
            if file.endswith('.nasl'): #Check only plugins file that ends with .nasl extension
                list_file_path = os.path.join(root, file)
                with open(list_file_path, 'rb') as list_file:
                    f_content = list_file.read()
                    oids_list.append(look_for_data(f_content, file))
    write_dataset_json(outputfile, oids_list)

if __name__ == "__main__":
    OPENVASFILE = 'OpenvasPlugins/PluginsOpenvas.tar.bz2'
    OPENVASDESTFILE = OPENVASFILE.replace('.tar.bz2', "")
    CONFIGS = 'OpenvasConfigs/OpenvasConfig.ini'
    OUTPUTJSONFILE = 'DatasetInput/OpenvasLookup.json'
    download_file(OPENVASFILE, CONFIGS)
    decompress_plugin_file(OPENVASDESTFILE, OPENVASFILE)
    walk_directories(OPENVASDESTFILE, OUTPUTJSONFILE)
