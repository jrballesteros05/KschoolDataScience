"""This module just read a config file where basically has the url where plugins are located
and donwload it, I also implemented the function compress if you want a dataset compressed"""
import configparser
import tarfile
import requests

def get_openvas_config_values(configfile):
    """
    Read the configuration file and return its values
    Parameters:
    ----------
    configfile : A .ini config file with the url to download openvas plugins

    Returns:
    -------
    dataframe_without_info : config values
    """
    config = configparser.ConfigParser()
    config.read(configfile)
    return config

def get_openvas_values(configfile):
    """
    Read the URL and return its base, this function was desinged for APIS
    Parameters:
    ----------
    configfile : read a config variable

    Returns:
    -------
    base_url : URL to download the data
    """
    config = get_openvas_config_values(configfile)
    base_url = config['Openvas']['URL']
    return base_url

def download_file(plugins_output_file, configfile):
    """
    Download the file from the website
    Parameters:
    ----------
    plugins_output_file : file object to download the data
    configfile : .ini file with configurations
    """
    url = get_openvas_values(configfile)
    response = requests.get(url)
    if response.status_code == 200:
        print('Response OK, downloading file from openvas web')
        with open(plugins_output_file, 'wb') as file_directory:
            for chunk in response.iter_content(chunk_size=128):
                file_directory.write(chunk)
        print('File download has been completed')

def decompress_plugin_file(dirpath, plugins_file):
    """
    Descompress a tar.bz2 file
    Parameters:
    ----------
    dirpath : Destination directory
    plugins_file : .tar.bz2 file to descompress
    """
    print('Descompressing file %s' % plugins_file) # Must be the absolute file path
    tar = tarfile.open(plugins_file)
    tar.extractall(path=dirpath)
    tar.close()
    print("Descompress has been completed")
