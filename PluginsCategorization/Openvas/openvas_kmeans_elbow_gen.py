"""This script is very simple, it just import the dataset OpenvasLookup.json,
loads as dataframe and get sum of squared distances from different k values.
"""
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from openvas_data_preprocessing import load_and_clean_dataframe
from openvas_data_preprocessing import apply_tfidf_raw_data
from openvas_data_preprocessing import preproccess_data


def get_metrics(k_range, jobs, data):
    """
    Test different values of k and get the sum of squared distances
    and finally put into array
    Parameters:
    ----------
    k_range : custom range to evaluate.
    jobs : Number of jobs, depends on your cpu cores
    data: Numeric data to modelate

    Returns:
    ---------
    metrics: return a the sum of squared distances and silohouette score,
    euclidean and cosine, metrics separated by commas.
    """
    metrics = []
    for iteration in range(1, 2):
        for k_value in k_range:
            k_model = KMeans(n_clusters=k_value, n_jobs=jobs)
            k_model.fit(data)
            label = k_model.labels_
            sil_coeff_euc = silhouette_score(data, label, metric='euclidean')
            sil_coeff_cos = silhouette_score(data, label, metric='cosine')
            iteration = str(iteration)
            k_val = str(k_value)
            sosd = str(str(k_model.inertia_)) # Sum of squared distances
            sil_coeff_euc = str(sil_coeff_euc)
            sil_coeff_cos = str(sil_coeff_cos)
            line = iteration + ',' + k_val + ',' + sosd + ',' + sil_coeff_euc + ',' + sil_coeff_cos
            metrics.append(line)
    return metrics

def write_results(outputfile, metrics_list):
    """
    Receives and array and write into a file
    Parameters:
    ----------
    outputfile : name of the outputfile
    metrics_list : list of metrics

    Returns:
    ---------
    file : filename with metrics for future processing
    """
    with open(outputfile, 'w') as outputf:
        for line in metrics_list:
            outputf.write("%s\n" % line)

if __name__ == "__main__":
    DATASET_FILE = 'DatasetInput/OpenvasLookup.json'
    RAW_OUTPUT_FILE = 'DatasetInput/k_values_metrics_raw.txt'
    PROCESSED_OUTPUT_FILE = 'DatasetInput/k_values_metrics_processed.txt'
    OPENVAS_VULNS_DF = load_and_clean_dataframe(DATASET_FILE)
    TFS_RAW = apply_tfidf_raw_data(OPENVAS_VULNS_DF, 'plugin_name')
    METRICS_RAW = get_metrics(range(2, 4), -1, TFS_RAW)
    write_results(RAW_OUTPUT_FILE, METRICS_RAW)
    OPENVAS_VULNS_DF_PROCESSED = load_and_clean_dataframe(DATASET_FILE)
    TFS_PROCESSED = preproccess_data(OPENVAS_VULNS_DF_PROCESSED)
    TFS_PROCESSED = apply_tfidf_raw_data(TFS_PROCESSED, 'pre_processed')
    METRICS_PROCESSED = get_metrics(range(2, 4), -1, TFS_PROCESSED)
    write_results(PROCESSED_OUTPUT_FILE, METRICS_PROCESSED)
